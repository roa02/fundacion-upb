﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Entidades;
using Entidades.Common;

namespace UPB.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<ApplicationRole> _rolManager;
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;

        public AccountController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<ApplicationRole> rolManager,
            ApplicationDbContext context,
            IConfiguration configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _rolManager = rolManager;
            _configuration = configuration;
            _context = context;
        }

        [Route("Users")]
        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            var Users = new List<UserInfo>();
            var Usuarios = _userManager.Users.Where(x => x.Activo == true).ToList();
            Usuarios.ForEach(x => Users.Add(new UserInfo { Nombre = x.Nombre, Email = x.Email, Id = x.Id, Activo = x.Activo, Login = x.UserName, Telefono = x.PhoneNumber, Cargo = x.Cargo, IdUniversitario = x.IdUniversitario }));

            return Json(new { success = true, message = Users });
        }

        [Route("Users/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetUser([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            var Usuario = _userManager.FindByIdAsync(id).Result;
            var totalRoles = _rolManager.Roles.ToList();
            var Roles = _userManager.GetRolesAsync(Usuario).Result.ToList();
            var user = new UserInfo { Id = Usuario.Id, Email = Usuario.Email, Login = Usuario.UserName, Nombre = Usuario.Nombre, Activo = Usuario.Activo, Telefono = Usuario.PhoneNumber, Cargo = Usuario.Cargo, IdUniversitario = Usuario.IdUniversitario };
            totalRoles.ForEach(x => user.LstRoles.Add(new Rol { Nombre = x.Name, Select = Roles.Any(y => y == x.Name) ? true : false }));

            return Json(new { success = true, message = user });
        }

        [Route("UsersUniversitarios")]
        [HttpGet]
        public async Task<IActionResult> GetUsersUniversitarios()
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            var Users = new List<UserInfo>();
            var Usuarios = _userManager.Users.Where(x => x.Activo == true).ToList();

            foreach (var Usuario in Usuarios)
            {
                var totalRoles = _rolManager.Roles.ToList();
                var Roles = _userManager.GetRolesAsync(Usuario).Result.ToList();
                var user = new UserInfo { Id = Usuario.Id, Email = Usuario.Email, Login = Usuario.UserName, Nombre = Usuario.Nombre, Activo = Usuario.Activo, Telefono = Usuario.PhoneNumber, Cargo = Usuario.Cargo, IdUniversitario = Usuario.IdUniversitario };
                totalRoles.ForEach(x => user.LstRoles.Add(new Rol { Nombre = x.Name, Select = Roles.Any(y => y == x.Name) ? true : false }));

                var rolAdmin = user.LstRoles.Where(x => x.Nombre == "Administrador" && x.Select == true).FirstOrDefault();
                if (rolAdmin == null)
                {
                    Users.Add(new UserInfo { Nombre = user.Nombre, Email = user.Email, Id = user.Id, Activo = user.Activo, Login = user.Login, Telefono = user.Telefono, Cargo = user.Cargo, IdUniversitario = user.IdUniversitario });
                }
            }

            return Json(new { success = true, message = Users });
        }

        [Route("Roles")]
        [HttpGet]
        public async Task<IActionResult> GetRoles()
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            var Roles = new List<Rol>();
            var Rols = _rolManager.Roles.ToList();
            Rols.ForEach(x => Roles.Add(new Rol { Nombre = x.Name, Select = false }));

            return Json(new { success = true, message = Roles });
        }

        [Route("Roles/{idRol}")]
        [HttpGet]
        public async Task<IActionResult> GetRolById([FromRoute] string idRol)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            var Rol = _rolManager.Roles.SingleOrDefault(x => x.Id == idRol);

            return Json(new { success = true, message = Rol });
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Administrador")]
        [Route("Create")]
        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] UserInfo model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = ErrorModelValidation.ShowError(new SerializableError(ModelState).Values) });
            }

            try
            {
                var resultUser = _context.Users.Where(x => x.IdUniversitario == model.IdUniversitario).FirstOrDefault();
                if (resultUser == null)
                {
                    var user = new ApplicationUser { UserName = model.Login, Nombre = model.Nombre, Email = model.Email, Codigo = model.Codigo, PhoneNumber = model.Telefono, Cargo = model.Cargo, Activo = true, IdUniversitario = model.IdUniversitario };
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        await AsignarRolAsync(_context, user, model.LstRoles);
                        return Json(new { success = true, message = "Usuario creado correctamente" });
                    }
                    else
                    {
                        string ErrorMsj = "";
                        result.Errors.ToList().ForEach(x => ErrorMsj += ErrorPassword(x.Code));
                        return Json(new { success = false, message = ErrorMsj });
                    }
                }
                else
                {
                    return Json(new { success = false, message = "El Id univesitario ya está registrado." });
                }
            }
            catch (Exception exc)
            {
                string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                return Json(new { success = false, message = "Error!. " + ErrorMsg });
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Administrador")]
        [Route("EditUser/{id}")]
        [HttpPut]
        public async Task<IActionResult> EditUser([FromRoute] string id, [FromBody] UserInfo model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = ErrorModelValidation.ShowError(new SerializableError(ModelState).Values) });
            }

            try
            {
                var Usuario = _userManager.FindByIdAsync(id).Result;

                //cambio de informacion basica
                Usuario.Nombre = model.Nombre;
                Usuario.Cargo = model.Cargo;
                Usuario.PhoneNumber = model.Telefono;
                _context.SaveChanges();
                //var Token = await _userManager.GeneratePasswordResetTokenAsync(Usuario);

                //cambio de roles                 
                await AsignarRolAsync(_context, Usuario, model.LstRoles);

                return Json(new { success = true, message = "Registro editado correctamente." });
            }
            catch (Exception exc)
            {
                string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                return Json(new { success = false, message = ErrorMsg });
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("EditPersonalInfo")]
        [HttpPut]
        public async Task<IActionResult> EditPersonalInfo([FromBody] UserInfo model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = ErrorModelValidation.ShowError(new SerializableError(ModelState).Values) });
            }

            try
            {
                var Usuario = _userManager.FindByIdAsync(User.getUserId()).Result;

                //cambio de informacion basica
                Usuario.Nombre = model.Nombre;
                Usuario.Cargo = model.Cargo;
                Usuario.PhoneNumber = model.Telefono;
                _context.SaveChanges();

                return Json(new { success = true, message = "Registro editado correctamente." });
            }
            catch (Exception exc)
            {
                string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                return Json(new { success = false, message = ErrorMsg });
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Administrador")]
        [Route("Delete/{IdUsuario}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteUser([FromRoute] string IdUsuario)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = ErrorModelValidation.ShowError(new SerializableError(ModelState).Values) });
            }

            try
            {
                var Usuario = _userManager.FindByIdAsync(IdUsuario).Result;
                Usuario.Activo = false;
                _context.SaveChanges();

                return Json(new { success = true, message = "Registro eliminado correctamente." });
            }
            catch (Exception exc)
            {
                string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                return Json(new { success = false, message = ErrorMsg });
            }

        }

        private async Task<IActionResult> AsignarRolAsync(ApplicationDbContext _context, ApplicationUser user, List<Rol> listaRoles)
        {
            try
            {
                var Roles = _context.Roles.ToList();

                foreach (var rol in Roles)
                {
                    bool RolSeleccionado = listaRoles.Where(x => x.Nombre == rol.Name).Select(x => x.Select).FirstOrDefault();

                    if (RolSeleccionado)
                    {
                        bool isInRole = _userManager.IsInRoleAsync(user, rol.Name).Result;
                        if (!isInRole)
                            await _userManager.AddToRoleAsync(user, rol.Name);
                    }
                    else
                    {
                        bool isInRole = _userManager.IsInRoleAsync(user, rol.Name).Result;
                        if (isInRole)
                            await _userManager.RemoveFromRoleAsync(user, rol.Name);
                    }
                }

                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "Registro editado correctamente." });

            }
            catch (Exception exc)
            {
                string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                return Json(new { success = false, message = ErrorMsg });
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("ChangePassword")] //Cambia la contraseña del usuario en la sesion 
        [HttpPost]
        public async Task<ActionResult> ChangePassword([FromBody] ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = ErrorModelValidation.ShowError(new SerializableError(ModelState).Values) });
            }

            try
            {
                string Id = User.getUserId();
                if (Id == null)
                    return Json(new { success = false, message = "Usuario no identificado, intente loguearse nuevamente." });

                var usuarioActual = _userManager.FindByIdAsync(Id).Result;

                if (usuarioActual != null)
                {

                    var result = await _userManager.ChangePasswordAsync(usuarioActual, model.OldPassword, model.NewPassword);

                    if (result.Succeeded)
                    {
                        return Json(new { success = true, message = "Contraseña cambiada satisfactoriamente." });
                    }
                    else
                    {
                        return Json(new { success = false, message = "La contraseña antigua es incorrecta." });
                    }
                }
                else
                {
                    return Json(new { success = false, message = "El usuario no se encuentra logueado." });
                }

            }
            catch (Exception exc)
            {
                string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                return Json(new { success = false, message = ErrorMsg });
            }
        }

        //*****Requerir rol de administrador*********
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Administrador")]
        [Route("ForcePassword")] //Cambia la contraseña de cualquier usuario
        [HttpPost]
        public async Task<ActionResult> ForcePassword([FromBody] EditPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = ErrorModelValidation.ShowError(new SerializableError(ModelState).Values) });
            }

            try
            {
                var user = _userManager.FindByIdAsync(model.Id).Result;
                var Token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var result = await _userManager.ResetPasswordAsync(user, Token, model.Password);

                if (result.Succeeded)
                {
                    return Json(new { success = true, message = "La contraseña se ha cambiado satisfactoriamente." });
                }
                else
                {
                    string ErrorMsj = "";
                    result.Errors.ToList().ForEach(x => ErrorMsj += ErrorPassword(x.Code));
                    return Json(new { success = false, message = ErrorMsj });
                }

            }
            catch (Exception excError)
            {
                return Json(new { success = false, message = excError.Message });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] UserInfo userInfo)
        {
            if (ModelState.IsValid)
            {
                var usuario = userInfo.Email.Contains('@') ? _userManager.FindByEmailAsync(userInfo.Email).Result : _userManager.FindByNameAsync(userInfo.Email).Result;
                var result = await _signInManager.PasswordSignInAsync(usuario, userInfo.Password, isPersistent: false, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    try
                    {
                        return BuildToken(userInfo);
                    }
                    catch (Exception exc)
                    {
                        string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                        return Json(new { success = false, message = "No se pudo obtener la clave de la variable de entorno " + ErrorMsg });
                    }
                }
                else
                {
                    return Json(new { success = false, message = "Usuario o contraseña invalido." });
                }
            }
            else
            {
                return Json(new { success = false, message = ErrorModelValidation.ShowError(new SerializableError(ModelState).Values) });
            }
        }

        //Implementación manual
        private IActionResult BuildToken(UserInfo userInfo)
        {
            try
            {
                ApplicationUser Usuario;
                // Adding roles code
                // Roles property is string collection but you can modify Select code if it it's not 
                if (userInfo.Email.Contains('@'))
                    Usuario = _userManager.FindByEmailAsync(userInfo.Email).Result;
                else
                    Usuario = _userManager.FindByNameAsync(userInfo.Email).Result;

                var roles = _userManager.GetRolesAsync(Usuario).Result;

                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.UniqueName, userInfo.Email),
                    new Claim(ClaimTypes.NameIdentifier, Usuario.Id), //*** Este Claim es interpretado en ExtensionMethod.cs para obtener el user.Id ***
                    new Claim(ClaimTypes.GivenName, Usuario.Nombre), //*** Este Claim es interpretado en ExtensionMethod.cs para obtener el Nombre
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };

                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token");
                claimsIdentity.AddClaims(roles.Select(role => new Claim(ClaimTypes.Role, role)));

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetConnectionString("Llave_secreta").ToString()));

                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var expiration = DateTime.UtcNow.AddDays(1);
                var codigos = claimsIdentity.Claims;

                JwtSecurityToken token = new JwtSecurityToken(
                   issuer: _configuration.GetConnectionString("serverDomain"),
                   audience: _configuration.GetConnectionString("serverDomain"),
                   claims: claimsIdentity.Claims,
                   expires: expiration,
                   signingCredentials: creds);

                return Ok(new
                {
                    success = true,
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = expiration,
                    nombre = Usuario.Nombre,
                    id = Usuario.Id,
                    roles = roles
                });
            }
            catch (Exception exc)
            {
                string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                return Json(new { success = false, message = "Error 503." + ErrorMsg });
            }
        }

        [Route("ListarPorRol/{NombreRol}")]
        [HttpGet]
        public async Task<IActionResult> ListarPorRol([FromRoute] string NombreRol)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            var Usuarios = _userManager.Users.ToList();
            var lstResult = new List<UserInfo>();

            foreach (var Usuario in Usuarios)
            {
                var Roles = _userManager.GetRolesAsync(Usuario).Result.ToList();
                foreach (var Rol in Roles)
                {
                    if (Rol.ToLower().Trim() == NombreRol.ToLower().Trim())
                    {
                        lstResult.Add(new UserInfo { Nombre = Usuario.Nombre, Id = Usuario.Id, Email = Usuario.Email, Activo = Usuario.Activo, Codigo = Usuario.Codigo });
                    }
                }
            }

            return Ok(lstResult);
        }

        public async Task<IActionResult> Unauthorized()
        {
            return Json(new { success = false, message = "Unauthorized" });
        }

        public async Task<IActionResult> Forbidden()
        {
            return Json(new { success = false, message = "Forbidden" });
        }

        private string ErrorPassword(string errorCode)
        {
            if (errorCode == "PasswordTooShort")
            {
                return "La contraseña debe contener al menos 6 caracteres. ";
            }
            else if (errorCode == "PasswordRequiresNonAlphanumeric")
            {
                return "La contraseña debe contener al menos un caracter no alfanúmerico. ";
            }
            else if (errorCode == "PasswordRequiresLower")
            {
                return "La contraseña debe contener al menos un caracter en minúscula. ";
            }
            else if (errorCode == "PasswordRequiresUpper")
            {
                return "La contraseña debe contener al menos un caracter en mayúscula. ";
            }
            else if (errorCode == "PasswordRequiresDigit")
            {
                return "La contraseña debe contener al menos un digito. ";
            }
            else if (errorCode == "DuplicateUserName")
            {
                return "En nombre de usuario ya se ha registrado. ";
            }
            else if (errorCode == "DuplicateEmail")
            {
                return "El email ya se encuentra registrado. ";
            }
            else if (errorCode == "InvalidUserName")
            {
                return "El campo usuario ingresado no es valido ";
            }
            else
            {
                return errorCode;
            }
        }
    }
}