﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entidades;
using Entidades.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;

namespace UPB.Controllers
{
    [Produces("application/json")]
    [Route("api/HorasLaborSocial")]
    public class HorasLaborSocialController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _env;

        public HorasLaborSocialController(ApplicationDbContext context, IHostingEnvironment env)
        {
            _context = context;
            _env = env;
        }

        // GET: api/HorasLaborSocial
        [HttpGet]
        public async Task<IActionResult> GetHorasLaborSocial()
        {
            try
            {
                return Json(new { success = true, message = _context.HorasLaborSocial.Include(x => x.User).ToList() });
            }
            catch (Exception exc)
            {
                string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                return Json(new { success = false, message = "Error!. " + ErrorMsg });
            }
        }

        // GET: api/HorasLaborSocial/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetHoraLaborSocial([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = ErrorModelValidation.ShowError(new SerializableError(ModelState).Values) });
            }

            try
            {
                var horaLaborSocial = await _context.HorasLaborSocial.Include(x => x.User).SingleOrDefaultAsync(m => m.Id == id);
                if (horaLaborSocial == null)
                {
                    return Json(new { success = false, message = "no se ha encontrado el registro." });
                }
                return Json(new { success = true, message = horaLaborSocial });
            }
            catch (Exception exc)
            {
                string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                return Json(new { success = false, message = "Error!. " + ErrorMsg });
            }
        }

        // PUT: api/HorasLaborSocial/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHoraLaborSocial([FromRoute] int id, [FromBody] HoraLaborSocial horaLaborSocial)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = ErrorModelValidation.ShowError(new SerializableError(ModelState).Values) });
            }

            try
            {
                if (id != horaLaborSocial.Id)
                {
                    return Json(new { success = false, message = "No se puede editar el identificador principal del registro." });
                }

                _context.Entry(horaLaborSocial).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "El registro se ha editado correctamente." });
            }
            catch (Exception exc)
            {
                string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                return Json(new { success = false, message = "Ocurrio un error al intentar guardar el registro en la base de datos." + ErrorMsg });
            }
        }

        // POST: api/HorasLaborSocial
        [HttpPost]
        public async Task<IActionResult> PostHoraLaborSocial([FromBody] HoraLaborSocial horaLaborSocial)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = ErrorModelValidation.ShowError(new SerializableError(ModelState).Values) });
            }

            try
            {
                _context.HorasLaborSocial.Add(horaLaborSocial);
                await _context.SaveChangesAsync();
                return Json(new { success = true, message = "El registro se ha guardado correctamente.", Id = horaLaborSocial.Id });
            }
            catch (Exception exc)
            {
                string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                return Json(new { success = false, message = "Ocurrio un error al intentar guardar el registro en la base de datos." + ErrorMsg });
            }
        }

        // POST: api/HorasLaborSocial/Documento/5
        [Route("Documento/{Id}/{IdUniversitario}")]
        [HttpPost]
        public async Task<IActionResult> PostArchivos(IFormFile file, [FromRoute] int Id, [FromRoute] string IdUniversitario)
        {
            string filename, fileExt, filenameFull;

            //validacion file
            if (file == null)
            {
                return Json(new { success = false, message = "No se ha adjuntado ningún archivo." });
            }

            string rutaProyecto = _env.WebRootPath;
            //logica de almacenamiento
            try
            {
                var uploadDir = Path.Combine("DocumentosLaborSocial", IdUniversitario);
                var rutaCompleta = Path.Combine(rutaProyecto, uploadDir);

                if (!Directory.Exists(rutaCompleta)) //Verifica si existe la carpeta y sino se crea
                {
                    Directory.CreateDirectory(rutaCompleta);
                }

                var registro = _context.HorasLaborSocial.Where(x => x.Id == Id).FirstOrDefault();

                //Modificar o Guarda dependinedo del caso
                fileExt = file.FileName.Trim('"').Split('.')[1];
                filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"').Split('.')[0];
                filenameFull = file.FileName;

                registro.Documento = Path.Combine(uploadDir, filenameFull);
                _context.Entry(registro).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                //se guarda el archivo en la carpeta
                using (FileStream fs = System.IO.File.Create(Path.Combine(rutaCompleta, filenameFull)))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }

                return Json(new { success = true, message = "El archivo adjunto se ha guardado correctamente." });
            }
            catch (Exception exc)
            {

                string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                return Json(new { success = false, message = "Ocurrio un error al realizar el proceso. " + rutaProyecto + ": "  + ErrorMsg });
            }
        }

        // DELETE: api/HorasLaborSocial/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHoraLaborSocial([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = ErrorModelValidation.ShowError(new SerializableError(ModelState).Values) });
            }

            try
            {
                var horaLaborSocial = await _context.HorasLaborSocial.SingleOrDefaultAsync(m => m.Id == id);
                if (horaLaborSocial == null)
                {
                    return Json(new { success = false, message = "no se ha encontrado el registro." });
                }

                _context.HorasLaborSocial.Remove(horaLaborSocial);
                await _context.SaveChangesAsync();

                return Json(new { success = true, message = "El registro se ha eliminado correctamente." });
            }
            catch (Exception exc)
            {
                string ErrorMsg = exc.GetBaseException().InnerException != null ? exc.GetBaseException().InnerException.Message : exc.GetBaseException().Message;
                return Json(new { success = false, message = "Ocurrio un error al intentar eliminar el registro en la base de datos. " + ErrorMsg });
            }
        }
    }
}